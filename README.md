### Description
This is a tool I wrote that takes .map files generated by sdcc (specifically when using GBDK)
and converts them into a .sym format used by BGB and is printed to stdout.

### Usage
```
$ convert_map_to_sym memory.map > memory.sym
```
